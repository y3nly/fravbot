import win32gui
import cv2
import numpy as np
from screenfix import get_screen_buffer, make_image_from_buffer
from pathlib import Path


class Screen:
    """
    Grabs and analyzes window screen.

    :param hwnd: A handle to a window.

    :ivar hwnd: A handle to a window.
    :ivar method: Template matching method to use.
    :ivar threshold: Threshold for the image match.
    :ivar templates: Grayscale template images.
    """

    def __init__(self, hwnd):
        self.hwnd = hwnd
        self.method = cv2.TM_SQDIFF_NORMED
        self.threshold = 0.1
        self.templates = {}
        p = Path('.')
        for img in p.glob('templates/*.jpg'):
            self.templates[img.stem] = cv2.imread(str(img), 0)

    def capture_window(self):
        """
        Grab an image of the application window.
        :return: A PIL Image as a Numpy array.
        """
        bitmap = get_screen_buffer(win32gui.GetWindowRect(self.hwnd))
        img = np.array(make_image_from_buffer(bitmap))
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        return gray

    def current_screen(self):
        """
        Returns the name of the screen the window is currently on.
        :return: Name of the screen if a match is found from the templates folder.
        """
        img = self.capture_window()
        for key in self.templates:
            res = cv2.matchTemplate(img, self.templates[key], self.method)
            min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
            if min_val < self.threshold:
                return key, min_loc

        return None, None
