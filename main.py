import win32gui
import win32con
import time
from controller import FravController
from screen import Screen
from flask import Flask, render_template, request
from random import randint

app = Flask(__name__)
running = False


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/start', methods=['GET', 'POST'])
def start():
    global running
    if not running:
        running = True
        print("start")
        mhwnd = win32gui.FindWindow(None, "World of Warcraft")
        # chwnd = win32gui.GetWindow(mhwnd, win32con.GW_CHILD)

        controller = FravController(mhwnd)
        # controller.press_key(0x30)
        # controller.click(20, 0)

        screen = Screen(mhwnd)

        while True:
            current, loc = screen.current_screen()
            if current == "alterac":
                controller.enter(loc[0] + 130, loc[1] + 80)
            elif current == "warsong":
                controller.enter(loc[0] + 130, loc[1] + 80)
            else:
                print("running")
            time.sleep(randint(3, 5))
    else:
        return "already started"


@app.route('/stop', methods=['GET', 'POST'])
def stop():
    print("stop")
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
