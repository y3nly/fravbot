$(document).ready(function () {
    $("button#start").click(function () {
        $.ajax({
            type: "POST",
            url: "/start"
        });
    });
    $("button#stop").click(function () {
        $.ajax({
            type: "POST",
            url: "/stop"
        });
    });
});